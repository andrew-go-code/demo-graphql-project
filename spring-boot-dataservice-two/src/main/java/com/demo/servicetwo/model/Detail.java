package com.demo.servicetwo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "details")
public class Detail {
    @Id
    private Integer id;

    @Column(name = "limit_id")
    private Integer limitId;

    @Column(name = "info")
    private String info;
}
