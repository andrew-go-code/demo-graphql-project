package com.demo.gateway.model.graph;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DetailItem {
    private Integer id;
    private Integer limitId;
    private String info;
}
