package com.demo.gateway.model.graph;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class LimitContainer {
    private List<LimitItem> report;
    private String exportFile;
}
