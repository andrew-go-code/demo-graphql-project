package com.demo.gateway.model.graph;

public enum LimitType {
    TYPE_ONE, TYPE_TWO;
}
